﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;
namespace BUS
{
    public class BUS_DanhSachXeChay
    {
        public static List<DTO_DanhSachXeChay> DanhSachXeChay(DTO_ThongTinChay TT)
        {
            List<DTO_DanhSachXeChay> ds = DAO_DanhSachXeChay.DanhSachXeChay(TT);
            return ds;
        }
    }
}
