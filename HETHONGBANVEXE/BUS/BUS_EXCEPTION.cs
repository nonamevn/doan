﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUS
{
    class BUS_EXCEPTION:ApplicationException
    {
        public BUS_EXCEPTION()
        {
        }

       public BUS_EXCEPTION(string Message):base(Message)
        {
        }
          
        public BUS_EXCEPTION(string message, Exception inner)
        : base(message, inner)
        {
        }

    }
}
