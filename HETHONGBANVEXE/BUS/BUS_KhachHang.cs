﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAO;
namespace BUS
{
    public class BUS_KhachHang
    {
        public static List<DTO_KhachHang> XemThongTinKhachHang(string TenKhachHang)
        {
            List<DTO_KhachHang> ds = DAO_KhachHang.ThongTinKhachHang(TenKhachHang);
            return ds;
        }
    }
}
