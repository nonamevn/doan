﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using DTO;
namespace BUS
{
    public class BUS_TaiKhoan
    {
        public static DTO_TaiKhoan KiemTraDangNhap(string TenTaiKhoan, string MatKhau)
        {
            DTO_TaiKhoan tk = new DTO_TaiKhoan();
            tk = DAO_TaiKhoan.DangNhap(TenTaiKhoan, MatKhau);
            return tk;
        }
    }
}
