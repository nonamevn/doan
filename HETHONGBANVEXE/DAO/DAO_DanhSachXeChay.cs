﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data.SqlClient;
using System.Data;

namespace DAO
{
    public class DAO_DanhSachXeChay
    {
        public static List<DTO_DanhSachXeChay> DanhSachXeChay(DTO_ThongTinChay TT)
        {
            List<DTO_DanhSachXeChay> list = new List<DTO_DanhSachXeChay>();
            DTO_DanhSachXeChay XeChay = new DTO_DanhSachXeChay();

            SqlConnection con = DataProvider.TaoKetNoi();
            try
            {
                con.Open();
            }
            catch
            {

            }
            string sql = "USP_DanhSachXeChay";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@BenDi";
            param.SqlDbType = SqlDbType.NVarChar;
            param.Size = 1024;
            param.Direction = ParameterDirection.Input;
            param.Value = "SÀI GÒN";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@BenDen";
            param.SqlDbType = SqlDbType.NVarChar;
            param.Size = 1024;
            param.Direction = ParameterDirection.Input;
            param.Value = "HÀ NỘI";
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@NgayDi";
            param.SqlDbType = SqlDbType.Date;
            param.Direction = ParameterDirection.Input;
            param.Value = "2015/12/12";
            cmd.Parameters.Add(param);

            try
            {
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while(dr.Read())
                    {
                        XeChay.MaXe = dr["MaXe"].ToString();
                        XeChay.TenTaiXe = dr["TenTaiXe"].ToString();
                        XeChay.TenTuyen = dr["TenTuyen"].ToString();
                        XeChay.BienSoXe = dr["BienSoXe"].ToString();
                        XeChay.TongSoGhe = Int32.Parse(dr["TongSoGhe"].ToString());
                        XeChay.SoGheTrong = Int32.Parse(dr["SoGheTrong"].ToString());
                        XeChay.GioChay = DateTime.Parse(dr["GioChay"].ToString());
                        list.Add(XeChay);
                    }
                }                
            }
            catch
            {

            }
            con.Close();
            return list;
        }
    }
}
