﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data.SqlClient;
using System.Data;

namespace DAO
{
    public class DAO_KhachHang
    {
        public static List<DTO_KhachHang> ThongTinKhachHang(string TenKhachHang)
        {
            List<DTO_KhachHang> list = new List<DTO_KhachHang>();
            DTO_KhachHang KH = new DTO_KhachHang();
            SqlConnection con = DataProvider.TaoKetNoi();
            try
            {
                con.Open();
                string sql = "USP_TimKhachHang";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@TenKhachHang";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Size = 1024;
                param.Direction = ParameterDirection.Input;
                param.Value = TenKhachHang;
                cmd.Parameters.Add(param);

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while(dr.Read())
                    {
                        KH.MAKH = dr["MAKH"].ToString();
                        KH.TENKH = dr["TENKH"].ToString();
                        KH.CMND = dr["CMND"].ToString();
                        KH.SDT = dr["SDT"].ToString();
                        KH.DIACHI = dr["DIACHI"].ToString();
                        KH.NGAYSINH = DateTime.Parse(dr["NGAYSINH"].ToString());
                        KH.GIOITINH = dr["GIOITINH"].ToString();
                        list.Add(KH);
                    }
                }
                con.Close();
            }
            catch
            {

            }
            return list;
        }
    }
}
