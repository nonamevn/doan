﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
namespace DAO
{
    public class DAO_PhieuGuiHang
    {
        public static Boolean ThemPhieuGuiHang(DTO_GuiHang GH)
        {
            Boolean check;

            SqlConnection con = DataProvider.TaoKetNoi();
            con.Open();
            string sql = "USP_ThemPhieuGuiHang";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@TenKhachHang";
            param.SqlDbType = SqlDbType.NVarChar;
            param.Size = 1024;
            param.Direction = ParameterDirection.Input;
            param.Value = GH.TenKhachHang;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@CMND";
            param.SqlDbType = SqlDbType.Char;
            param.Size = 10;
            param.Direction = ParameterDirection.Input;
            param.Value = GH.CMND;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@SDT";
            param.SqlDbType = SqlDbType.Char;
            param.Size = 11;
            param.Direction = ParameterDirection.Input;
            param.Value = GH.SDT;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@TenNguoiNhan";
            param.SqlDbType = SqlDbType.NVarChar;
            param.Size = 1024;
            param.Direction = ParameterDirection.Input;
            param.Value = GH.TenNguoiNhan;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@SDTNguoiNhan";
            param.SqlDbType = SqlDbType.Char;
            param.Size = 11;
            param.Direction = ParameterDirection.Input;
            param.Value = GH.SDTNguoiNhan;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@NoiNhan";
            param.SqlDbType = SqlDbType.NVarChar;
            param.Size = 1024;
            param.Direction = ParameterDirection.Input;
            param.Value = GH.NoiNhan;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@MaTiepTan";
            param.SqlDbType = SqlDbType.Char;
            param.Size = 10;
            param.Direction = ParameterDirection.Input;
            param.Value = GH.MaTiepTan;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@MaCa";
            param.SqlDbType = SqlDbType.Char;
            param.Size = 10;
            param.Direction = ParameterDirection.Input;
            param.Value = GH.MaCa;
            cmd.Parameters.Add(param);
            
            try
            {
                cmd.ExecuteNonQuery();
                check = true;
            }
            catch
            {
                check = false;
            }
            con.Close();
            return check;
        }
    }
}
