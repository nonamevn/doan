﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data.SqlClient;
using System.Data;

namespace DAO
{
    public class DAO_TaiKhoan
    {
        
        public static DTO_TaiKhoan DangNhap(string TenTaiKhoan, string MatKhau)
        {
            DTO_TaiKhoan TaiKhoan = new DTO_TaiKhoan();
            SqlConnection con = DataProvider.TaoKetNoi();
            con.Open();
            string sql = "USP_DangNhap";
            SqlCommand cmd = new SqlCommand(sql,con);
            cmd.CommandType = CommandType.StoredProcedure;
            

            SqlParameter param = new SqlParameter();
            param.ParameterName = "@TenTaiKhoan";
            param.SqlDbType = SqlDbType.Char;
            param.Size = 100;
            param.Direction = ParameterDirection.Input;
            param.Value = TenTaiKhoan;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@MatKhau";
            param.SqlDbType = SqlDbType.Char;
            param.Size = 1024;
            param.Direction = ParameterDirection.Input;
            param.Value = MatKhau;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@Quyen";
            param.SqlDbType = SqlDbType.Int;
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);

            param = new SqlParameter();
            param.ParameterName = "@MaNV";
            param.SqlDbType = SqlDbType.Char;
            param.Size = 10;
            param.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(param);

            try
            {
                cmd.ExecuteNonQuery();

                
                TaiKhoan.Quyen = (int)cmd.Parameters["@Quyen"].Value;
                TaiKhoan.MaNV = (string)cmd.Parameters["@MaNV"].Value;
            }
            catch
            {

            }

            con.Close();
            return TaiKhoan;
        }
    }
}
