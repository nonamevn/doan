﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_DanhSachXeChay
    {
        private string _MaXe;
        private string _TenTaiXe;
        private string _TenTuyen;
        private string _BienSoXe;
        private int _TongSoGhe;
        private int _SoGheTrong;
        private DateTime _GioChay;

        public string MaXe
        {
            get { return _MaXe; }
            set { _MaXe = value; }
        }
        public string TenTaiXe
        {
            get { return _TenTaiXe; }
            set { _TenTaiXe = value; }
        }
        public string TenTuyen
        {
            get { return _TenTuyen; }
            set { _TenTuyen = value; }
        }
        public string BienSoXe
        {
            get { return _BienSoXe; }
            set { _BienSoXe = value; }
        }
        public int TongSoGhe
        {
            get { return _TongSoGhe; }
            set { _TongSoGhe = value; }
        }
        public int SoGheTrong
        {
            get { return _SoGheTrong; }
            set { _SoGheTrong = value; }
        }
        public DateTime GioChay
        {
            get { return _GioChay; }
            set { _GioChay = value; }
        }
        private ObservableCollection<DTO_DanhSachXeChay> danhSachXe;

        public ObservableCollection<DTO_DanhSachXeChay> ListXe
        {
            get { return danhSachXe; }
            set { danhSachXe = value; }
        }
 
        public DTO_DanhSachXeChay(List<DTO_DanhSachXeChay> ds)
        {
            danhSachXe = new ObservableCollection<DTO_DanhSachXeChay>();
            for (int i = 0; i < ds.Count(); i++)
            {
                DTO_DanhSachXeChay item = new DTO_DanhSachXeChay();
                item.MaXe = ds[i].BienSoXe;
                item.TenTaiXe = ds[i].TenTaiXe;
                item.TenTuyen = ds[i].TenTuyen;
                item.TongSoGhe = ds[i].TongSoGhe;
                item.SoGheTrong = ds[i].SoGheTrong;
                item.GioChay = ds[i].GioChay;
                danhSachXe.Add(item);
            }
        }

        public DTO_DanhSachXeChay()
        {
        }
    }
}
