﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_GuiHang
    {       
        private string _TenKhachHang;
        private string _CMND;
        private string _SDT;
        private string _TenNguoiNhan;
        private string _SDTNguoiNhan;
        private string _NoiNhan;
        private string _MaTiepTan;
        private string _MaCa;
        public string TenKhachHang
        {
            get { return _TenKhachHang; }
            set { _TenKhachHang = value; }
        }
        public string CMND
        {
            get { return _CMND; }
            set { _CMND = value; }
        }
        public string SDT
        {
            get { return _SDT; }
            set { _SDT = value; }
        }
        public string TenNguoiNhan
        {
            get { return _TenNguoiNhan; }
            set { _TenNguoiNhan = value; }
        }
        public string SDTNguoiNhan
        {
            get { return _SDTNguoiNhan; }
            set { _SDTNguoiNhan = value; }
        }
        public string NoiNhan
        {
            get { return _NoiNhan; }
            set { _NoiNhan = value; }
        }
        public string MaTiepTan
        {
            get { return _MaTiepTan; }
            set { _MaTiepTan = value; }
        }
        public string MaCa
        {
            get { return _MaCa; }
            set { _MaCa = value; }
        }

        public DTO_GuiHang()
        {

        }
    }
}
