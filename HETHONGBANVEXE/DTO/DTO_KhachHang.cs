﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_KhachHang
    {
        private string _MaKH;
        private string _TenKH;
        private string _CMND;
        private string _SDT;
        private string _DiaChi;
        private string _GioiTinh;
        private DateTime _NgaySinh;

        public string MAKH
        {
            get { return _MaKH; }
            set { _MaKH = value; }
        }
        public string TENKH
        {
            get { return _TenKH; }
            set { _TenKH = value; }
        }
        public string CMND
        {
            get { return _CMND; }
            set { _CMND = value; }
        }
        public string SDT
        {
            get { return _SDT; }
            set { _SDT = value; }
        }
        public string DIACHI
        {
            get { return _DiaChi; }
            set { _DiaChi = value; }
        }
        public string GIOITINH
        {
            get { return _GioiTinh; }
            set { _GioiTinh = value; }
        }
        public DateTime NGAYSINH
        {
            get { return _NgaySinh; }
            set { _NgaySinh = value; }
        }

        private ObservableCollection<DTO_KhachHang> _KH;

        public ObservableCollection<DTO_KhachHang> KH
        {
            get { return _KH; }
            set { _KH = value; }
        }
        public DTO_KhachHang()
        {

        }
        public DTO_KhachHang(List<DTO_KhachHang> ds)
        {
            //Day la ham dựng khởi tạo thông tin cho xe
            _KH = new ObservableCollection<DTO_KhachHang>();
            for (int i = 0; i < ds.Count(); i++)
            {              
                _KH.Add(ds[i]);
            }
        }
    }
}
