﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_PhieuGuiHang
    {
        private string _MaPG;
        private DateTime _NgayDat;
        private string _TenNguoiNhan;
        private string _SDTNguoiNhan;
        private string _NoiNhan;
        private string _MaTiepTan;
        private string _MaCa;
        private string _MaKH;
        public string MaPG
        {
            get { return _MaPG; }
            set { _MaPG = value; }
        }
        public DateTime NgayDat
        {
            get { return _NgayDat; }
            set { _NgayDat = value; }
        }
        public string TenNguoiNhan
        {
            get { return _TenNguoiNhan; }
            set { _TenNguoiNhan = value; }
        }
        public string SDTNguoiNhan
        {
            get { return _SDTNguoiNhan; }
            set { _SDTNguoiNhan = value; }
        }
        public string NoiNhan
        {
            get { return _NoiNhan; }
            set { _NoiNhan = value; }
        }
        public string MaTiepTan
        {
            get { return _MaTiepTan; }
            set { _MaTiepTan = value; }
        }
        public string MaCa
        {
            get { return _MaCa; }
            set { _MaCa = value; }
        }
        public string MaKH
        {
            get { return _MaKH; }
            set { _MaKH = value; }
        }
    }
}
