﻿namespace DTO
{
    public class DTO_TaiKhoan
    {
        private string _TenTaiKhoan;
        private string _MatKhau;
        private int _Quyen;
        private string _MaNV;
        public string TenTaiKhoan
        {
            get { return _TenTaiKhoan; }
            set { _TenTaiKhoan = value; }
        }
        public string MatKhau
        {
            get { return _MatKhau; }
            set { _MatKhau = value; }
        }
        public int Quyen
        {
            get { return _Quyen; }
            set { _Quyen = value; }
        }
        public string MaNV
        {
            get { return _MaNV; }
            set { _MaNV = value; }
        }
        public DTO_TaiKhoan()
        {

        }
    }
}
