﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DTO_ThongTinChay
    {
        private string _BenDi;
        private string _BenDen;
        private DateTime _NgayDi;

        public string BenDi
        {
            get { return _BenDi; }
            set { _BenDi = value; }
        }
        public string BenDen
        {
            get { return _BenDen; }
            set { _BenDen = value; }
        }
        public DateTime NgayDi
        {
            get { return _NgayDi; }
            set { _NgayDi = value; }
        }
        public DTO_ThongTinChay()
        {

        }
    }
}
