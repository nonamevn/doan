﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GUI;
namespace GUI.ModelView
{
    class GheModel
    {
        int maGheThucTe;
        int maGheAo;
        int trangThai;
        string tenGhe;

        public string pr_tenGhe
        {
            get { return tenGhe; }
            set { tenGhe = value; }
        }

        public int pr_maGheThucTe
        {
            get { return maGheThucTe; }
            set { maGheThucTe = value; }
        }
        public int pr_maGheAo
        {
            get { return maGheAo; }
            set { maGheAo = value; }
        }
        public int pr_trangThai
        {
            get { return trangThai; }
            set { trangThai = value; }
        }
    }

    class DanhSachGhe
    {
        private ObservableCollection<GheModel> danhSachGhe;

        public ObservableCollection<GheModel> ListGhe
        {
            get { return danhSachGhe; }
        }
        /// <summary>
        /// Ham Dung
        /// </summary>
        public DanhSachGhe()
        {
            danhSachGhe = new ObservableCollection<GheModel>();

            int soGhe = 45;
            if (soGhe == 45)
            {
                int k = 0;
                for (int i = 1; i <= soGhe + 15; i++)
                {
                    GheModel item = new GheModel();
                    item.pr_maGheAo = i;
                    if ((i <= 24 || i >= 36) && i != 1 && i != 13 && i != 37 && i != 49)
                    {
                        k++;
                    }
                    item.pr_maGheThucTe = k;
                    item.pr_tenGhe = "Ghế " + k.ToString();
                    item.pr_trangThai = 3;
                    if (i == 1 || i == 49)
                        item.pr_trangThai = -1;
                    danhSachGhe.Add(item);
                }
            }

        }
    }

}
