﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI.ModelView
{
    class XeModel
    {
        int maXe;
        int soLuongGhe;
        string nguoiLai;
        string bienSo;

        public int pr_maXe
        {
            get { return maXe; }
            set { maXe = value; }
        }
        public int pr_soLuongGhe
        {
            get { return soLuongGhe; }
            set { soLuongGhe = value; }
        }
        public string pr_nguoiLai
        {
            get { return nguoiLai; }
            set { nguoiLai = value; }
        }
        public string pr_bienSo
        {
            get { return bienSo; }
            set { bienSo = value; }
        }
    }
    class DanhSachXe
    {
        private ObservableCollection<XeModel> danhSachXe;

        public ObservableCollection<XeModel> ListXe
        {
            get { return danhSachXe; }
        }
        /// <summary>
        /// Ham Dung
        /// </summary>
        public DanhSachXe()
        {
            danhSachXe = new ObservableCollection<XeModel>();
                for (int i = 1; i <= 5; i++)
                {
                    XeModel item = new XeModel();
                    item.pr_maXe = i;
                    item.pr_nguoiLai = "Nguoi Lai: " + i.ToString();
                    danhSachXe.Add(item);
                }
            }

        }
}

