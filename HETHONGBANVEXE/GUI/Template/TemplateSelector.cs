﻿using GUI.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace GUI.Template
{
    public class MyStyle_Ghe_Selector : StyleSelector
    {
        public override Style SelectStyle(object item, DependencyObject container)
        {
            GheModel obj = item as GheModel;
            FrameworkElement element = container as FrameworkElement;
            int check = obj.pr_maGheAo;

            if (obj != null && element != null)
            {
                if (check == 1)
                {
                    return Application.Current.FindResource("styleCua_TaiXe") as Style;
                }
                else
                {
                    if (check == 13 || check >= 25 & check <= 35 || check == 37)
                    {
                        return Application.Current.FindResource("style_LoiDi") as Style;
                    }
                    if (check == 49)
                    {
                        return Application.Current.FindResource("styleCua_TaiXe") as Style;
                    }
                }
            }
            return null;
        }
    }

    public class MyDataTemplate_Ghe_Selector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            GheModel obj = item as GheModel;
            FrameworkElement element = container as FrameworkElement;
            int check = obj.pr_maGheAo;

            if (obj != null && element != null)
            {
                if (check < 2)
                {
                    return (DataTemplate)element.FindResource("myDataTemplate_Cua") as DataTemplate;
                }
                else
                {
                    if(check==49)
                    {
                        return (DataTemplate)element.FindResource("myDataTemplate_TaiXe") as DataTemplate;
                    }
                    return (DataTemplate)element.FindResource("myDataTemplate_Ghe") as DataTemplate;
                }
            }
            return null;
        }
    }
}
