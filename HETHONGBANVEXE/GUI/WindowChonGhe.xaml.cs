﻿using GUI.ModelView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI
{
    /// <summary>
    /// Interaction logic for WindowChonGhe.xaml
    /// </summary>
    public partial class WindowChonGhe : Window
    {
        public WindowChonGhe()
        {
            InitializeComponent();
            // DataContext = new DanhSachGhe();
            lstGhe.ItemsSource = new DanhSachGhe().ListGhe;
            Init();
        }

        public void Init()
        {
            soGheDaChon.Text = "0";
        }

        public T GetChildOfType<T>(DependencyObject depObj)
    where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetChildOfType<T>(child);
                if (result != null) return result;
            }
            return null;
        }
        private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        private void btnChonGheXong_Click(object sender, RoutedEventArgs e)
        {
           
            for (int i = 0; i < lstGhe.SelectedItems.Count; i++)
            {
                ListBoxItem myListBoxItem = (ListBoxItem)(lstGhe.ItemContainerGenerator.ContainerFromItem(lstGhe.SelectedItems[i]));

                if (myListBoxItem != null)
                {
                    ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(myListBoxItem);
                    DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;

                    string MaXe="";
                    TextBlock maXe = (TextBlock)myDataTemplate.FindName("nameGhe", myContentPresenter);
                    MaXe += maXe.Text.ToString();
                    MessageBox.Show(MaXe, "Mã Số Ghế");

                }
                else
                {
                    MessageBox.Show("Mày chưa chọn ghế", "Thông Báo");
                }
            }
        }

        private void lstGhe_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //soLuongGheDaChon.Text = lstGhe.SelectedItems.Count.ToString();
            Binding bb = new Binding("SelectedItems.Count");
            bb.Source = lstGhe;
            soGheDaChon.SetBinding(TextBlock.TextProperty, bb);
        }
    }
}


