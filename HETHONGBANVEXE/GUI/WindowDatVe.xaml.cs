﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GUI.ModelView;
namespace GUI
{
    /// <summary>
    /// Interaction logic for WindowDatVe.xaml
    /// </summary>
    public partial class WindowDatVe : Window
    {
        public WindowDatVe()
        {
            InitializeComponent();
            lbXe.ItemsSource = new DanhSachXe().ListXe;
        }

        private void btnManHinhChonGhe_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem myListBoxItem = (ListBoxItem)(lbXe.ItemContainerGenerator.ContainerFromItem(lbXe.SelectedItem));
            if (myListBoxItem!=null)
            {
                ContentPresenter myContentPresenter = FindVisualChild<ContentPresenter>(myListBoxItem);
                DataTemplate myDataTemplate = myContentPresenter.ContentTemplate;

                string MaXe;
                TextBlock maXe = (TextBlock)myDataTemplate.FindName("maXe", myContentPresenter);
                MaXe = maXe.Text.ToString();
                MessageBox.Show(MaXe, "Mã Số Xe");
                WindowChonGhe chonGhe = new WindowChonGhe();
                chonGhe.Show();
            }
            else
            {
                MessageBox.Show("Mày chưa chọn xe","Thông Báo");
            }
            
        }
        private childItem FindVisualChild<childItem>(DependencyObject obj) where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
    }
}